<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Información';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $datos ?>
    </p>
</div>
