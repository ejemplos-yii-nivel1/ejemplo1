<?php

/* @var $this yii\web\View */

$this->title = 'Ejemplo numero 1';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Felicidades</h1>

        <p class="lead">Este es el primer ejemplo de nuestro curso</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><?= $datos["titulo"] ?></h2>

                <p><?= $datos["cuerpo"] ?></p>
            </div>
        </div>
    </div>
</div>
