<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Html;




class SiteController extends Controller {
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout'],
//                'rules' => [
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
//    public function actions()
//    {
//        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
//        ];
//    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index', [
                    "datos" => [
                        "titulo" => "Primer bloque",
                        "cuerpo" => "Este es el texto del primer bloque"
                    ]
        ]);
    }

    public function actionAbout() {
        return $this->render('about', [
                    "datos" => "Información enviada desde el controlador"
        ]);
    }

    public function actionFoto() {
        return $this->render('foto', [
                    "imagen" => "1.jpg"
        ]);
    }
    
    public function colocarFoto($nombre){
        return Html::img("@web/imgs/$nombre",["class"=>"center-block img-responsive"]);
    }

}
