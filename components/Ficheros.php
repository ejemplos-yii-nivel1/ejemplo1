<?php
 
	namespace app\components;
        use Yii;
        use yii\helpers\FileHelper;
        use yii\base\Component;
		
	class Ficheros extends Component{
            public $carpeta;
            
            
            public function init(){
                    parent::init();
                    $this->carpeta=".";
                }
		
                public function listar($directorio=null) {
			if(!empty($directorio)){
                            $this->carpeta=$directorio;
                        }
                        $listado=FileHelper::findFiles($this->carpeta);
                        return $listado;
		}
                
                public function tamano($archivo=null){
                    if(empty($archivo)){
                        return $this->listar();
                    }
                    return filesize($archivo);
                }
	
	}

