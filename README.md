<p align="center">
    <h1 align="center">Ejemplo numero 1 de Yii - Nivel 1</h1>
    <br>
</p>

En este ejemplo vamos a partir del layout base y vamos a realizar en una serie de commits los siguientes cambios:
<ul>
<li>Realizando pequeño cambios en la configuracion</li>
<li>Colocar un favicon</li>
<li>eliminar carpetas innecesarias</li>
<li>añadir css y js al Appset principal</li>
<li>definir una nueva vista y una accion en el controlador</li>
<li>Creando un nuevo componente</li>
<li>Creando un componente y colocarle en la configuracion</li>
</ul>

INSTALACION
------------

### INSTALANDO MEDIANTE COMPOSER

Teniendo Composer instalado, puedes instalar Yii ejecutando los siguientes 
comandos en un directorio accesible vía Web:

~~~
composer global require "fxp/composer-asset-plugin:^1.4.1"
composer create-project --prefer-dist yiisoft/yii2-app-basic ejemplo1Nivel1
~~~

El primer comando instala composer asset plugin, que permite administrar 
dependencias de paquetes bower y npm a través de Composer. 
Sólo necesitas ejecutar este comando una vez. El segundo comando instala Yii 
en un directorio llamado ejemplo1Nivel1. 

Puedes elegir un nombre de directorio diferente si así lo deseas.



